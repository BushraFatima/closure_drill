// Importing module.
let limitFunctionCallCount = require ('../limitFunctionCallCount.cjs');
// Defining callback function.
function toBeCalled(param) {
    return param;
}
// Log result or Log error if any.
try {
    let finalResult = limitFunctionCallCount(toBeCalled, 4);
    console.log(finalResult());

} catch (error) {
    console.log(error);
}





