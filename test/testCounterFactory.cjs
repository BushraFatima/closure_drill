// Importing module.
const counterFactory = require ('../counterFactory.cjs');
// Storing resultant object from counterFactory to finalResult.
const finalResult = counterFactory();
// Calling increase function to increase the counter variable.
console.log(finalResult.increment());
// Calling decrease function to decrease the counter variable.
console.log(finalResult.decrement());
