function cacheFunction(cb) {
    let cache = {};

    function invoke(...args) {
        let argArray = JSON.stringify(args);

        if (argArray in cache) {
            return cache[argArray];

        } else {
            cache[argArray] = cb(...args);
            return cache[argArray];
        } 
    }

    if (typeof cb !== 'function') {
        throw new Error('Provide valid parameter');

    } else {
        return invoke;
    }
        
}

module.exports = cacheFunction;