function limitFunctionCallCount(cb, n) {
    let count = 0;

    function invoke(...args) {

        if (count < n) {
            count += 1;
            return cb(...args);

        } else {
            return null;
        }    

    }

    if (typeof cb !== 'function' || typeof n !== 'number' || n <= 0) {
        throw new Error('Provide valid parameters');
    } else {
        return invoke;
    }

}

module.exports = limitFunctionCallCount;